<?php
	
namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class VivifyController extends Controller
{
	public function indexAction()
	{
		return new Response('Hello world!');
	}
	
	public function sayHelloAction($name)
	{
		return new Response('Hello ' . $name . '!');
	}
}